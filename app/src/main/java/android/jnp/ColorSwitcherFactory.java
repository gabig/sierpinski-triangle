package android.jnp;

public class ColorSwitcherFactory {
    public final int vertexLight;
    public final int vertexMedium;
    public final int vertexDark;
    public final int colorsNumber;

    ColorSwitcherFactory(int vertexLight, int vertexDark) {
        this.vertexLight = vertexLight;
        this.vertexMedium = 0;
        this.vertexDark = vertexDark;
        this.colorsNumber = 2;
    }

    ColorSwitcherFactory(int vertexLight, int vertexMedium, int vertexDark) {
        this.vertexLight = vertexLight;
        this.vertexMedium = vertexMedium;
        this.vertexDark = vertexDark;
        this.colorsNumber = 3;
    }

    public ColorSwitcher create() {
        if (colorsNumber == 2) {
            return new ColorSwitcher(vertexLight, vertexDark);
        } else {
            return new ColorSwitcher(vertexLight, vertexMedium, vertexDark);
        }
    }
}

package android.jnp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.List;

public class DrawingView extends View {
    private Board board;
    private Paint p;
    private Path path;
    private int boardSize = 1;
    private int numberOfColors = 2;
    private int lightVertexColor;
    private int mediumVertexColor;
    private int darkVertexColor;
    private int edgeColor;
    private int width;
    private int height;

    public DrawingView(Context context) {
        super(context);
        lightVertexColor = Color.LTGRAY;
        mediumVertexColor = Color.DKGRAY;
        darkVertexColor = Color.BLACK;
        edgeColor = Color.BLUE;
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setStrokeWidth(10);
        path = new Path();
        updateBoard();
    }

    public DrawingView(Context context, AttributeSet attributeSet) {
        this(context);
    }

    public DrawingView(Context context, AttributeSet attributeSet, int defStyle) {
        this(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (board != null) {
            List<Edge> edgesToDraw = board.getEdges();
            p.setStyle(Paint.Style.STROKE);
            p.setColor(board.getEdgeColor());
            for (Edge edgeToDraw : edgesToDraw) {
                path.rewind();
                path.moveTo(edgeToDraw.getX1(), edgeToDraw.getY1());
                path.lineTo(edgeToDraw.getX2(), edgeToDraw.getY2());
                canvas.drawPath(path, p);
            }

            List<Vertex> verticesToDraw = board.getVertices();
            for (int i = 0; i < verticesToDraw.size(); i++) {
                p.setStyle(Paint.Style.FILL);
                p.setColor(board.getVertexColor(i));
                canvas.drawCircle(
                        verticesToDraw.get(i).getX(), verticesToDraw.get(i).getY(),
                        board.getVertexSize(), p);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        if(event.getAction() == MotionEvent.ACTION_DOWN && board != null) {
            board.onTouch(x, y);
            invalidate();
            if (board.isSolved()) {
                board.scramble();
            }
        }
        return true;
    }

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public void setNumberOfColors(int numberOfColors) {
        this.numberOfColors = numberOfColors;
    }

    public void updateBoard() {
        ColorSwitcherFactory colorSwitcherFactory;
        if (numberOfColors == 2) {
            colorSwitcherFactory =
                    new ColorSwitcherFactory(lightVertexColor, darkVertexColor);
        }
        else {
            colorSwitcherFactory = new ColorSwitcherFactory(
                    lightVertexColor, mediumVertexColor, darkVertexColor);
        }
        board = new Board(boardSize, width, height, edgeColor, colorSwitcherFactory);
        board.scramble();
        invalidate();
    }
}
package android.jnp;

import java.util.ArrayList;
import java.util.List;

public class BoardGenerator {
    private final int boardSize;
    private final int unitWidth;
    private final int unitHeight;
    private final int width;
    private final int height;
    private List<Edge> edges;
    private List<Vertex> vertices;
    private final ColorSwitcherFactory colorSwitcherFactory;
    private final CoordsToVertex coordsToVertex = new CoordsToVertex();

    BoardGenerator(
            int width, int height, int boardSize, ColorSwitcherFactory colorSwitcherFactory) {
        this.boardSize = boardSize;
        this.unitHeight = height / (int)(Math.pow(2.0, (double)boardSize) + 2);
        this.unitWidth = width / (int)(Math.pow(2.0, (double) boardSize) + 2);
        this.colorSwitcherFactory = colorSwitcherFactory;
        this.width = width;
        this.height = height;
        this.edges = new ArrayList<Edge>();
        this.vertices = new ArrayList<Vertex>();
        generateBoard();
    }

    private void generateBoard() {
        generateBoard(width / 2, unitHeight, 1);
    }

    private void generateBoard(int x, int y, int depth) {
        if (depth == boardSize) {
            Vertex upper = getVertex(x, y);
            Vertex left = getVertex(x - unitWidth, y + 2 * unitHeight);
            Vertex right = getVertex(x + unitWidth, y + 2 * unitHeight);
            Vertex middleLeft = getVertex(x - (unitWidth / 2), y + unitHeight);
            Vertex middleRight = getVertex(x + (unitWidth / 2), y + unitHeight);
            Vertex middleBottom = getVertex(x, y + 2 * unitHeight);
            addEdge(upper, middleLeft);
            addEdge(upper, middleRight);
            addEdge(left, middleLeft);
            addEdge(left, middleBottom);
            addEdge(right, middleRight);
            addEdge(right, middleBottom);
            addEdge(middleBottom, middleLeft);
            addEdge(middleBottom, middleRight);
            addEdge(middleLeft, middleRight);
        } else if (depth < boardSize) {
            int actWidth = (int) (unitWidth * Math.pow(2.0, (double) boardSize - depth));
            int actHeight = (int) (unitHeight * Math.pow(2.0, (double) boardSize - depth));
            generateBoard(x, y, depth + 1);
            generateBoard(x - (actWidth / 2), y + actHeight, depth + 1);
            generateBoard(x + (actWidth / 2), y + actHeight, depth + 1);
        }
    }

    private void addEdge(Vertex a, Vertex b) {
        Edge edge = new Edge(a.getX(), a.getY(), b.getX(), b.getY());
        a.addNeighbour(b);
        b.addNeighbour(a);
        edges.add(edge);
    }

    private Vertex getVertex(int x, int y) {
        Vertex vertex = coordsToVertex.get(x, y);
        if (vertex == null) {
            vertex = new Vertex(x, y, colorSwitcherFactory.create());
            coordsToVertex.put(x, y, vertex);
            vertices.add(vertex);
        }
        return vertex;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Edge> getEdges() {
        return edges;
    }
}

package android.jnp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    Spinner spinner_size, spinner_colors;
    String sizes[] = {"1", "2", "3"};
    String numberOfColors[] = {"2", "3"};
    ArrayAdapter<String> adapter_size, adapter_colors;
    private int chosenSize = 1;
    private int chosenNumberOfColors = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner_size = (Spinner)findViewById(R.id.spinner_size);
        adapter_size = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sizes);
        spinner_size.setAdapter(adapter_size);

        spinner_size.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                chosenSize = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner_colors = (Spinner)findViewById(R.id.spinner_colors);
        adapter_colors = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, numberOfColors);
        spinner_colors.setAdapter(adapter_colors);

        spinner_colors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                chosenNumberOfColors = position + 2;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startNewGame(View view) {
        Intent getBoardIntent = new Intent(this, BoardScreen.class);
        getBoardIntent.putExtra("boardSize", chosenSize);
        getBoardIntent.putExtra("numberOfColors", chosenNumberOfColors);
        startActivity(getBoardIntent);
    }
}
package android.jnp;

import java.util.List;
import java.util.Random;

public class Board {
    private final List<Edge> edges;
    private final List<Vertex> vertices;
    public final int edgeColor;
    public final int boardSize;
    public final int width;
    public final int height;
    public final int vertexSize;

    Board(int boardSize, int width, int height, int edgeColor,
          ColorSwitcherFactory colorSwitcherFactory) {
        this.boardSize = boardSize;
        this.width = width;
        this.height = height;
        this.edgeColor = edgeColor;
        if (boardSize == 3) {
            this.vertexSize = 40;
        } else {
            this.vertexSize = 50;
        }

        BoardGenerator boardGenerator;
        boardGenerator = new BoardGenerator(width, height, boardSize, colorSwitcherFactory);
        this.vertices = boardGenerator.getVertices();
        this.edges = boardGenerator.getEdges();
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public int getEdgeColor() {
        return edgeColor;
    }

    public int getVertexColor(int vertexNo) {
        if (0 <= vertexNo && vertexNo < vertices.size()) {
            return vertices.get(vertexNo).getColor();
        }
        throw new IndexOutOfBoundsException();
    }

    public void onTouchVertex(Vertex vertex) {
        vertex.changeColor();
        List<Vertex> neighbours = vertex.getNeighbours();

        for (Vertex neighbour : neighbours) {
            neighbour.changeColor();
        }
    }

    public void onTouch(int x, int y) {
        Vertex vertex = getVertexFromCoords(x, y);
        if (vertex == null) {
            return;
        }
        onTouchVertex(vertex);
    }

    private Vertex getVertexFromCoords(int x, int y) {
        for (Vertex vertex : vertices) {
            if (Math.abs(x - vertex.getX()) <= vertexSize &&
                    Math.abs(y - vertex.getY()) <= vertexSize) {
                return vertex;
            }
        }
        return null;
    }

    public boolean isSolved() {
        for (Vertex vertex : vertices) {
            if (!vertex.isSolved()) {
                return false;
            }
        }
        return true;
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public void scramble() {
        do {
            for (int i = 0; i < 50; i++) {
                Random random = new Random();
                int vertexNo = random.nextInt(vertices.size() - 1);
                onTouchVertex(vertices.get(vertexNo));
            }
        } while (this.isSolved());
    }

    public int getVertexSize() {
        return vertexSize;
    }
}

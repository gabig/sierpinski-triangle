package android.jnp;

import java.util.HashMap;
import java.util.Map;

public class CoordsToVertex {
    Map<Integer, Map<Integer, Vertex>> map = new HashMap();

    CoordsToVertex() {}

    public void put(int x, int y, Vertex vertex) {
        if (!map.containsKey(x)) {
            map.put(x, new HashMap<Integer, Vertex>());
        }
        map.get(x).put(y, vertex);
    }

    public Vertex get(int x, int y) {
        if (!map.containsKey(x)) {
            return null;
        }
        return map.get(x).get(y);
    }
}


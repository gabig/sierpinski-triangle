package android.jnp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

public class BoardScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.board_layout);
        Intent activityThatCalled = getIntent();

        int boardSize = activityThatCalled.getExtras().getInt("boardSize");
        int numberOfColors = activityThatCalled.getExtras().getInt("numberOfColors");

        LinearLayout boardLayout = findViewById(R.id.board_layout);
        DrawingView boardView = (DrawingView) boardLayout.getChildAt(0);
        boardView.setBoardSize(boardSize);
        boardView.setNumberOfColors(numberOfColors);
        boardView.updateBoard();
    }
}

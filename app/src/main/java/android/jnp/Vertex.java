package android.jnp;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
    private final int x;
    private final int y;
    private final ColorSwitcher colorSwitcher;
    private List<Vertex> neighbours = new ArrayList<Vertex>();

    Vertex(int x, int y, ColorSwitcher colorSwitcher) {
        this.x = x;
        this.y = y;
        this.colorSwitcher = colorSwitcher;
    }

    public int getColor() {
        return colorSwitcher.getColor();
    }

    public void changeColor() {
        colorSwitcher.changeColor();
    }

    public void addNeighbour(Vertex neighbour) {
        neighbours.add(neighbour);
    }

    public void addNeighbours(List<Vertex>neighbours) {
        for(Vertex neighbour : neighbours) {
            this.neighbours.add(neighbour);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public List<Vertex> getNeighbours() {
        return neighbours;
    }

    public boolean isSolved() {
        return colorSwitcher.isSolved();
    }
}

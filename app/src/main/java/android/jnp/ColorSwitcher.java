package android.jnp;

public class ColorSwitcher {
    public final int[] vertexColor;
    public final int numberOfColors;
    public int actColor;

    public ColorSwitcher(int vertexLight, int vertexMedium, int vertexDark) {
        vertexColor = new int[]{vertexLight, vertexMedium, vertexDark};
        actColor = 0;
        numberOfColors = 3;
    }

    public ColorSwitcher(int vertexLight, int vertexDark) {
        vertexColor = new int[]{vertexLight, vertexDark};
        actColor = 0;
        numberOfColors = 2;
    }

    public int getColor() {
        return vertexColor[actColor];
    }

    public void changeColor() {
        actColor++;
        actColor %= numberOfColors;
    }

    public boolean isSolved() {
        return (actColor + 1 == numberOfColors);
    }
}
